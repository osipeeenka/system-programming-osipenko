#include <stdio.h>

char print_format[] = "%d\n";
char error_mes[] = "error: overflow\n";
char A = 12;
char B = 18;
char zero = 0;
char array[] = {-13, 12, -14, 56, -19, 22, 19, 18, 7};

int main() {
	char *ptr = array;
	char cl = 0;
	char bl = *ptr;

	while (1) {
		if (A == bl) {
			ptr++;
			bl = *ptr;
			continue;
		}

		if (B == bl) {
			break;
		}

		if (bl < zero) {
			ptr++;
			bl = *ptr;
			continue;
		} 
		cl += bl;

		if (cl < 0) {
			printf("%s", error_mes);
			return 0;
		}

		ptr++;
		bl = *ptr;
	}

	*ptr = 0;
	*(ptr+1) = cl;

	printf(print_format, cl);

	return 0;
}
